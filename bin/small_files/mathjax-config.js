// latex config 
(function () {
    if (!window.MathJax) {
         window.MathJax = {
            loader: {load: ['[tex]/autoload', '[tex]/color','[tex]/boldsymbol','[tex]/mhchem','[tex]/verb','[tex]/physics']},
            tex: {
                packages: {'[+]': ['autoload','color','boldsymbol','mhchem','verb','physics']},
                inlineMath: [
                    ['$', '$']
                ],
                displayMath: [ // start/end delimiter pairs for display math
                    ['$$', '$$']
                ],
                processEscapes:false,
            },
            svg: {
                displayAlign: 'left',
                fontCache: 'global',
            },
            options: {
                renderActions: {
                    addMenu: [0, '', '']
                }
            },
            startup: {
                typeset: false,
                ready() {
                    // 部分 math 标签中的字符 解析会出现 't.setAttribute' is undefined 错误 ,
                    // 根据官方给出的解决方案 添加以下代码解决 
                    // 官方解决方案地址 https://github.com/mathjax/MathJax/issues/2402
                  if (MathJax.version === '3.0.5') {
                    const SVGWrapper = MathJax._.output.svg.Wrapper.SVGWrapper;
                    const CommonWrapper = SVGWrapper.prototype.__proto__;
                    SVGWrapper.prototype.unicodeChars = function (text, variant) {
                      if (!variant) variant = this.variant || 'normal';
                      return CommonWrapper.unicodeChars.call(this, text, variant);
                    }
                  }
                  MathJax.startup.defaultReady();
                }
            }
        };
    }
    var script = document.createElement('script');
    script.src = MOSO_LATEX_TEX_CHTML;
    script.async = true;
    document.head.appendChild(script);
})();